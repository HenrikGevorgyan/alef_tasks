<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\StudentController;
use \App\Http\Controllers\LessonController;
use \App\Http\Controllers\LessonLectureController;
use \App\Http\Controllers\LectureController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::prefix('students')->group(function () {
//    1) получить список всех студентов
    Route::get('/', [StudentController::class, 'getStudents']);
//    2) получить информацию о конкретном студенте (имя, email + класс + прослушанные лекции)
    Route::get('/{id}', [StudentController::class, 'getStudent']);
//    3) создать студента
    Route::post('/', [StudentController::class, 'createStudent']);
//    4) обновить студента (имя, принадлежность к классу)
    Route::put('/{id}', [StudentController::class, 'updateStudent']);
//    5) удалить студента
    Route::delete('/{id}', [StudentController::class, 'deleteStudent']);
});

/*
 * Class зарезервированное слово от Php невозможно использовать на Laravel!!!
 * Lessons это Classes!
 */
Route::prefix('lessons')->group(function () {
//    6) получить список всех классов
    Route::get('/', [LessonController::class, 'getLessons']);
//    7) получить информацию о конкретном классе (название + студенты класса)
    Route::get('/student-lesson/{id}', [LessonController::class, 'getStudentLesson']);
//    8) получить учебный план (список лекций) для конкретного класса
    Route::get('/student-lecture/{id}', [LessonController::class, 'getStudentlecture']);
//    9) создать/обновить учебный план (очередность и состав лекций) для конкретного класса
    Route::prefix('lesson-lecture')->group(function () {
        Route::post('/', [LessonLectureController::class, 'createLessonLectures']);
        Route::put('/', [LessonLectureController::class, 'updateLessonLectures']);
    });
//    10) создать класс
    Route::post('/', [LessonController::class, 'createLesson']);
//    11) обновить класс (название)
    Route::put('/{id}', [LessonController::class, 'updateLesson']);
//    12) удалить класс (при удалении класса, привязанные студенты должны открепляться от класса, но не удаляться полностью из системы)
    Route::delete('/{id}', [LessonController::class, 'destroyLesson']);
});

Route::prefix('lectures')->group(function () {
//    13) получить список всех лекций
    Route::get('/', [LectureController::class, 'getLectures']);
//    14) получить информацию о конкретной лекции (тема, описание + какие классы прослушали лекцию + какие студенты прослушали лекцию)
    Route::get('/{id}', [LectureController::class, 'getLecture']);
//    15) создать лекцию
    Route::post('/', [LectureController::class, 'createLecture']);
//    16) обновить лекцию (тема, описание)
    Route::put('/{id}', [LectureController::class, 'updateLecture']);
//    17) удалить лекцию
    Route::delete('/{id}', [LectureController::class, 'deleteLecture']);

});


