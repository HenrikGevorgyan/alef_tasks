<?php

namespace App\Providers;

use App\Contracts\BaseInterface;
use App\Contracts\LectureInterface;
use App\Contracts\LessonInterface;
use App\Contracts\LessonLectureInterface;
use App\Contracts\StudentInterface;
use App\Repositories\BaseRepository;
use App\Repositories\LectureRepository;
use App\Repositories\LessonLectureRepository;
use App\Repositories\LessonRepository;
use App\Repositories\StudentRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(
            BaseInterface::class,
            BaseRepository::class
        );
        $this->app->bind(
            StudentInterface::class,
            StudentRepository::class
        );
        $this->app->bind(
            LessonInterface::class,
            LessonRepository::class
        );
        $this->app->bind(
            LessonLectureInterface::class,
            LessonLectureRepository::class
        );
        $this->app->bind(
            LectureInterface::class,
            LectureRepository::class
        );
    }
}
