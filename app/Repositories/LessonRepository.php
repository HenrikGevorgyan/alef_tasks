<?php

namespace App\Repositories;

use App\Contracts\LessonInterface;
use App\Models\Lesson;
use \App\Models\Student;

class LessonRepository extends BaseRepository implements LessonInterface
{
    protected $model;

    public function __construct(Lesson $model)
    {
        parent::__construct($model);
    }

    public function getLessons()
    {
        return Lesson::get();
    }

    public function getStudentLesson(int $id)
    {
        return Lesson::with('students')->find($id);
    }

    public function getStudentlecture(int $id)
    {
        return Lesson::with('lectures')->find($id);
    }

    public function createLesson(array $params)
    {
        return Lesson::create($params);
    }

    public function updateLesson($params,int $id)
    {
        $lesson = Lesson::find($id);
        $lesson->update([
            'name' => $params['name'],
        ]);
        $lesson->save();
        return $lesson->only(['name']);
    }

    public function destroyLesson(int $id)
    {
        return Student::find($id)->delete();
    }
}
