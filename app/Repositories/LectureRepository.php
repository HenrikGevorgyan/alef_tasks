<?php

namespace App\Repositories;

use App\Contracts\LectureInterface;
use App\Models\Lecture;

class LectureRepository extends BaseRepository implements LectureInterface
{
    protected $model;

    public function __construct(Lecture $model)
    {
        parent::__construct($model);
    }

    public function getLectures()
    {
        return Lecture::get();
    }

    public function getLecture(int $id)
    {
        return Lecture::with('lessons')->find($id);
    }

    public function createLecture(array $params)
    {
        return Lecture::create($params);
    }

    public function updateLecture($params,int $id)
    {
        $lecture = Lecture::find($id);
        $lecture->update([
            'subject' => $params['subject'],
            'description' => $params['description'],
        ]);
        $lecture->save();
        return $lecture->only(['subject', 'description']);
    }

    public function deleteLecture(int $id)
    {
        return Lecture::find($id)->delete();
    }
}
