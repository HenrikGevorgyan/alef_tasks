<?php

namespace App\Repositories;

use App\Contracts\LessonLectureInterface;
use App\Models\Lecture;
use App\Models\Lesson;

class LessonLectureRepository implements LessonLectureInterface
{

    public function createLessonLectures($params)
    {
        $lesson = Lecture::find($params['lesson_id']);
        $lecture = Lesson::find($params['lecture_id']);
        return $lesson->lessons()->attach($lecture);
    }

    public function updateLessonLectures($params)
    {
        $lesson = Lecture::find($params['lesson_id']);
        $lecture = Lesson::find($params['lecture_id']);
        return $lesson->lessons()->sync($lecture);
    }
}
