<?php

namespace App\Repositories;

use App\Models\Lesson;
use Illuminate\Database\Eloquent\Model;
use \App\Contracts\StudentInterface;
use \App\Models\Student;
use \App\Repositories\BaseRepository;

class StudentRepository extends BaseRepository implements StudentInterface
{
    protected $model;

    public function __construct(Student $model)
    {
        parent::__construct($model);
    }

    public function getStudents()
    {
        return Student::get();
    }

    public function getStudent(int $id)
    {
        return Student::with('lessons')->find($id);
    }

    public function createStudent(array $params)
    {
        return Student::create($params);
    }

    public function updateStudent($params,int $id)
    {
        $student = Student::find($id);
        $student->update([
            'lesson_id' => $params['lesson_id'],
            'name' => $params['name'],
            'email' => $params['email'],
        ]);
        $student->save();
        return $student->only(['lesson_id', 'name', 'email']);
    }

    public function deleteStudent(int $id)
    {
        return Student::find($id)->delete();
    }
}
