<?php

namespace App\Http\Requests\Students;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lesson_id' => [
                'required',
                'int',
                'exists:lessons,id'
            ],
            'name' => [
                'required',
                'unique:students',
                'max:255'
            ],
            'email' => [
                'required',
                'unique:students',
                'max:255'
            ],
         ];
    }
}
