<?php

namespace App\Http\Requests\Lecture;

use Illuminate\Foundation\Http\FormRequest;

class CreateLectureRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => [
                'required',
                'string',
                'max:255',
            ],
            'description' => [
                'string',
                'string',
                'max:255'
            ],
        ];
    }
}
