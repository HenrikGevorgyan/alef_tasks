<?php

namespace App\Http\Requests\LessonLectures;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLessonLectureRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lesson_id' => [
                'required',
                'int',
                'exists:lessons,id',
            ],
            'lecture_id' => [
                'required',
                'int',
                'exists:lectures,id',
            ],
        ];

    }
}
