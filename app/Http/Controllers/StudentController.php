<?php

namespace App\Http\Controllers;

use App\Http\Requests\Students\CreateStudentRequest;
use App\Http\Requests\Students\UpdateStudentRequest;
use App\Repositories\StudentRepository;

class StudentController extends Controller
{
    protected $repository;

    public function __construct(StudentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getStudents()
    {
        return response()->json([
            'success' => true,
            'message' => 'get Students',
            'data' => $this->repository->getStudents(),
        ]);
    }

    public function getStudent(int $id)
    {
        return response()->json([
            'success' => true,
            'message' => 'get Student',
            'data' => $this->repository->getStudent($id),
        ]);
    }

    public function createStudent(CreateStudentRequest $request)
    {
        return response()->json([
            'success' => true,
            'message' => 'created has ben successfully Student',
            'data' =>  $this->repository->createStudent($request->all()),
        ]);
    }

    public function updateStudent(UpdateStudentRequest $request,int $id)
    {
        return response()->json([
            'success' => true,
            'message' => 'updated has ben successfully Student',
            'data' =>  $this->repository->updateStudent($request,$id)
        ]);
    }

    public function deleteStudent(int $id)
    {
        return response()->json([
            'success' => true,
            'message' => 'deleted has ben successfully Student',
            'data' =>  $this->repository->deleteStudent($id)
        ]);
    }
}
