<?php

namespace App\Http\Controllers;

use App\Http\Requests\Lecture\CreateLectureRequest;
use App\Http\Requests\Lecture\UpdateLectureRequest;
use App\Repositories\LectureRepository;
use Illuminate\Http\Request;

class LectureController extends Controller
{

    protected $repository;

    public function __construct(LectureRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getLectures()
    {
        return response()->json([
            'success' => true,
            'message' => 'get Lectures',
            'data' =>  $this->repository->getLectures()
        ]);
    }

    public function getLecture(int $id)
    {
        return response()->json([
            'success' => true,
            'message' => 'get Lecture',
            'data' =>  $this->repository->getLecture($id)
        ]);
    }

    public function createLecture(CreateLectureRequest $request)
    {
        return response()->json([
            'success' => true,
            'message' => 'created has ben successfully Lecture',
            'data' =>  $this->repository->createLecture($request->all()),
        ]);
    }

    public function updateLecture(UpdateLectureRequest $request,int $id)
    {
        return response()->json([
            'success' => true,
            'message' => 'updated has ben successfully Lecture',
            'data' =>  $this->repository->updateLecture($request,$id)
        ]);
    }

    public function deleteLecture(int $id)
    {
        return response()->json([
            'success' => true,
            'message' => 'deleted has ben successfully Lecture',
            'data' =>  $this->repository->deleteLecture($id)
        ]);
    }

}
