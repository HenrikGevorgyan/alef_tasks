<?php

namespace App\Http\Controllers;

use App\Http\Requests\LessonLectures\CreateLessonLectureRequest;
use App\Http\Requests\LessonLectures\UpdateLessonLectureRequest;
use App\Repositories\LessonLectureRepository;

class LessonLectureController extends Controller
{
    protected $repository;

    public function __construct(LessonLectureRepository $repository)
    {
        $this->repository = $repository;
    }

    public function createLessonLectures(CreateLessonLectureRequest $request)
    {
        return response()->json([
            'success' => true,
            'message' => 'Created has ben successfully, Lesson Lectures',
            'data' => $this->repository->createLessonLectures($request),
        ]);
    }

    public function updateLessonLectures(UpdateLessonLectureRequest $request)
    {
        return response()->json([
            'success' => true,
            'message' => 'Updated has ben successfully, Lesson Lectures',
            'data' => $this->repository->updateLessonLectures($request),
        ]);
    }
}
