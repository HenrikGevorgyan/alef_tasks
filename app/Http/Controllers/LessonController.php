<?php

namespace App\Http\Controllers;

use App\Http\Requests\Lessons\CreateLessonRequest;
use App\Http\Requests\Lessons\UpdateLessonRequest;
use App\Repositories\LessonRepository;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    protected $repository;

    public function __construct(LessonRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getLessons()
    {
        return response()->json([
            'success' => true,
            'message' => 'get lessons',
            'data' => $this->repository->getLessons(),
        ]);
    }

    public function getStudentLesson(int $id)
    {
        return response()->json([
            'success' => true,
            'message' => 'get student lesson',
            'data' => $this->repository->getStudentLesson($id),
        ]);
    }

    public function getStudentlecture(int $id)
    {
        return response()->json([
            'success' => true,
            'message' => 'get student lesson',
            'data' => $this->repository->getStudentlecture($id),
        ]);
    }

    public function createLesson(CreateLessonRequest $request)
    {
        return response()->json([
            'success' => true,
            'message' => 'created has ben successfully Lesson',
            'data' => $this->repository->createLesson($request->all()),
        ]);
    }

    public function updateLesson(UpdateLessonRequest $request,int $id)
    {
        return response()->json([
            'success' => true,
            'message' => 'updated has ben successfully Lesson',
            'data' => $this->repository->updateLesson($request,$id),
        ]);
    }

    public function destroyLesson(int $id)
    {
        return response()->json([
            'success' => true,
            'message' => 'destroyed has ben successfully Lesson',
            'data' => $this->repository->destroyLesson($id),
        ]);
    }

}
