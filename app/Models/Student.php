<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Student extends Model
{
    use HasFactory,softDeletes;

    protected $table = 'students';

    protected $fillable = [
        'lesson_id',
        'name',
        'email',
    ];

    protected $casts = [
        'lesson_id' => 'int',
        'name' => 'string',
        'email' => 'string',
    ];

    public function lessons()
    {
        return $this->belongsTo(Lesson::class,'lesson_id');
    }
}
