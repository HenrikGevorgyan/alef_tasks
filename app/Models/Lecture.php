<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{
    use HasFactory;

    protected $table = 'lectures';

    protected $fillable = [
        'subject',
        'description',
    ];

    protected $casts = [
        'subject' => 'string',
        'description' => 'string',
    ];

    public function lessons()
    {
        return $this->belongsToMany(Lesson::class,'lesson_lectures','lecture_id','lesson_id');
    }
}
